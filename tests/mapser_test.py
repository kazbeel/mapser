# -*- coding: utf-8 -*-
import os

import pytest
from mock import patch

from mapser.mapser import MapFileAnalyzer

# Getting rid of PyCharm's warning
patch.object = patch.object


@pytest.fixture(scope='module')
def get_asset_file(filename):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), 'assets', filename)


# noinspection PyClassHasNoInit
class TestMapFileLoader(object):
    def test_creation(self):
        pass

    def test_load_mapfile_not_exists(self):
        mapfile_loader = MapFileAnalyzer()
        with pytest.raises(IOError):
            mapfile_loader.load('nonexistentfile.map')

    def test_common_symbols_append_valid_value(self):
        expected = [
            {
                'name': 'name'
            }
        ]

        mapfile_loader = MapFileAnalyzer()
        mapfile_loader._common_symbol_append_or_update({'name': 'name'})

        assert mapfile_loader.common_symbols == expected

    def test_common_symbols_append_invalid_value(self):
        mapfile_loader = MapFileAnalyzer()
        mapfile_loader._common_symbol_append_or_update({'bad_key': 'value'})

        assert mapfile_loader.common_symbols == []

    def test_common_symbols_update_valid_value(self):
        expected = [
            {
                'name': 'name',
                'key1': 'value1'
            }
        ]

        mapfile_loader = MapFileAnalyzer()
        mapfile_loader.common_symbols.append({'name': 'name'})
        mapfile_loader._common_symbol_append_or_update({'name': 'name', 'key1': 'value1'})

        assert mapfile_loader.common_symbols == expected

    @patch.object(MapFileAnalyzer, '_process_cross_reference')
    @patch.object(MapFileAnalyzer, '_process_memory_map')
    @patch.object(MapFileAnalyzer, '_process_memory_configuration')
    @patch.object(MapFileAnalyzer, '_process_discarded_sections')
    @patch.object(MapFileAnalyzer, '_process_common_symbols')
    @patch.object(MapFileAnalyzer, '_process_archive_member')
    def test_file_dissection(self, archive_member, common_syms, discarded_sects, mem_conf, mem_map, cross_ref):
        with open(get_asset_file('sections.map'), 'r') as fd:
            mapfile_loader = MapFileAnalyzer()
            mapfile_loader._file_dissection(fd.read())

            archive_member.assert_called_once()
            archive_member.assert_called_with('\na 1   2\nb\nc\n\n')
            common_syms.assert_called_once()
            common_syms.assert_called_with('\n d 1\ne\n\n')
            discarded_sects.assert_called_once()
            discarded_sects.assert_called_with('\nf\n\n')
            mem_conf.assert_called_once()
            mem_conf.assert_called_with('\n1\n2\n\n3 a\n 4 b\n\n')
            mem_map.assert_called_once()
            mem_map.assert_called_with('\n\n5\n6\n7\n\n')
            cross_ref.assert_called_once()
            cross_ref.assert_called_with('\n\n\nz\n')

    def test_process_archive_member(self):
        with open(get_asset_file('archive_member_section.map'), 'r') as fd:
            mapfile_loader = MapFileAnalyzer()
            mapfile_loader._process_archive_member(fd.read())

            assert len(mapfile_loader.archive_members) == 0

    def test_process_common_symbols(self):
        expected = [
            {
                'name': 'GLB_goFlags',
                'size': 0x31,
                'path': './2_Programa/source',
                'object': 'objects.o',
                'library': 'application'
            },
            {
                'name': 'errno',
                'size': 0x4,
                'path': 'c:/freescale/kds_v3/toolchain/bin/../lib/gcc/arm-none-eabi/4.8.4/' +
                        '../../../../arm-none-eabi/lib/armv6-m',
                'object': 'lib_a-reent.o',
                'library': 'libg.a'
            },
            {
                'name': 'GLB_appIntObjPropChangeFlags',
                'size': 0x1,
                'path': './2_Programa/source',
                'object': 'interface_objects.o',
                'library': 'application'
            },
            {
                'name': 'tsiCtx',
                'size': 0x2d8,
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/tsi_k/libs',
                'object': 'tsi_k.o',
                'library': 'libtsi_k_v0_4.a'
            }
        ]

        with open(get_asset_file('common_symbols_section.map'), 'r') as fd:
            mapfile_loader = MapFileAnalyzer()
            mapfile_loader._process_common_symbols(fd.read())

            assert mapfile_loader.common_symbols == expected

    def test_process_discarded_sections(self):
        with open(get_asset_file('discarded_sections_section.map'), 'r') as fd:
            mapfile_loader = MapFileAnalyzer()
            mapfile_loader._process_discarded_sections(fd.read())

            assert len(mapfile_loader.discarded_sections) == 0

    def test_process_memory_configuration(self):
        expected = [
            {
                'name': 'm_sys_services',
                'origin': 0x00000300,
                'size': 0x00000100,
                'attributes': 'xr'
            },
            {
                'name': 'm_data',
                'origin': 0x20000400,
                'size': 0x00004c00,
                'attributes': 'rw'
            }
        ]

        with open(get_asset_file('memory_configuration.map'), 'r') as fd:
            mapfile_loader = MapFileAnalyzer()
            mapfile_loader._process_memory_configuration(fd.read())

            assert mapfile_loader.memory_regions == expected

    def test_process_memory_map(self):
        expected_stack = 0x400
        expected_heap = 0x100
        expected_app_files = [
            {
                'path': r'./2_Programa/source/',
                'object': 'access_control.o',
                'library': 'application'
            },
            {
                'path': r'./2_Programa/source/',
                'object': 'ad_inputs.o',
                'library': 'application'
            },
            {
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/root/libs/motion_sensor/v0_3_7/',
                'object': None,
                'library': 'libmotion_sensor_v0_3_7.a'
            },
            {
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/root/libs/heartbeat/v0_1_1/',
                'object': None,
                'library': 'libheartbeat_v0_1_1.a'
            },
            {
                'path': 'c:/freescale/kds_v3/toolchain/bin/../lib/gcc/arm-none-eabi/4.8.4/armv6-m/',
                'object': None,
                'library': 'libgcc.a'
            },
            {
                'path': 'c:/freescale/kds_v3/toolchain/bin/../lib/gcc/arm-none-eabi/4.8.4/armv6-m/',
                'object': None,
                'library': 'libgcc.a'
            }
        ]
        expected_memory_segments = [
            {
                'name': 'sys_services',
                'origin': 0x00000300,
                'size': 0x80
            },
            {
                'name': 'usr_services',
                'origin': 0x0000a000,
                'size': 0xf0
            },
            {
                'name': 'text',
                'origin': 0x0000a200,
                'size': 0xa48
            },
            {
                'name': 'iplt',
                'origin': 0x0000ac48,
                'size': 0x0
            },
            {
                'name': 'ARM',
                'origin': 0x0000ac48,
                'size': 0x0
            },
            {
                'name': 'bkup',
                'origin': 0x00037000,
                'size': 0x0
            },
            {
                'name': 'grot',
                'origin': 0x00038000,
                'size': 0x4
            },
            {
                'name': 'rel.dyn',
                'origin': 0x00040000,
                'size': 0x0
            },
            {
                'name': 'igot.plt',
                'origin': 0x20000400,
                'size': 0x0
            },
            {
                'name': 'bss',
                'origin': 0x20000400,
                'size': 0x490
            },
            {
                'name': 'save',
                'origin': 0x20005000,
                'size': 0x4
            },
            {
                'name': 'ARM.attributes',
                'origin': 0x00000000,
                'size': 0x30
            },
            {
                'name': 'debug_info',
                'origin': 0x00000000,
                'size': 0xa367
            },
            {
                'name': 'debug_str',
                'origin': 0x00000000,
                'size': 0x4b20
            },
            {
                'name': 'comment',
                'origin': 0x00000000,
                'size': 0x70
            },
            {
                'name': 'stabstr',
                'origin': 0x00000000,
                'size': 0x14d
            }
        ]
        expected_objects_information = [
            {
                'segment': 'sysServices',
                'origin': 0x00000300,
                'size': 0x80,
                'path': './2_Programa/source',
                'object': 'sys_services.o',
                'library': 'application',
                'symbol': 'GLB_sysServicesTable',
                'fill': 0
            },
            {
                'segment': 'usrServices$C',
                'origin': 0x0000a080,
                'size': 0x70,
                'path': './2_Programa/source',
                'object': 'usr_services.o',
                'library': 'application',
                'symbol': 'GLB_usrISRServicesTable',
                'fill': 0
            },
            {
                'segment': 'params',
                'origin': 0x0000a200,
                'size': 0x4,
                'path': './2_Programa/source',
                'object': 'app_parameters.o',
                'library': 'application',
                'symbol': 'GLB_appParams',
                'fill': 0
            },
            {
                'segment': 'rodata',
                'origin': 0x0000a624,
                'size': 0x40,
                'path': 'c:/freescale/kds_v3/toolchain/bin/../lib/gcc/arm-none-eabi/4.8.4/armv6-m',
                'object': 'divsf3.o',
                'library': 'libgcc.a',
                'symbol': '',
                'fill': 0
            },
            {
                'segment': 'rodata',
                'origin': 0x0000a664,
                'size': 0x40,
                'path': 'c:/freescale/kds_v3/toolchain/bin/../lib/gcc/arm-none-eabi/4.8.4/armv6-m',
                'object': 'mulsf3.o',
                'library': 'libgcc.a',
                'symbol': '',
                'fill': 0
            },
            {
                'segment': 'rodata',
                'origin': 0x0000a204,
                'size': 0x0f,
                'path': './2_Programa/source',
                'object': 'interface_objects.o',
                'library': 'application',
                'symbol': 'GLB_appIntObjLUT',
                'fill': 1
            },
            {
                'segment': 'rodata',
                'origin': 0x0000a2d0,
                'size': 0x18,
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/root/libs/scheduler/v0_6',
                'object': 'scheduler.o',
                'library': 'libscheduler_v0_6.a',
                'symbol': 'SCH_convertTime',
                'fill': 0
            },
            {
                'segment': 'text',
                'origin': 0x0000a2e8,
                'size': 0,
                'path': 'c:/freescale/kds_v3/toolchain/bin/../lib/gcc/arm-none-eabi/4.8.4/armv6-m',
                'object': '_udivsi3.o',
                'library': 'libgcc.a',
                'symbol': '__aeabi_uidiv',
                'fill': 0
            },
            {
                'segment': 'text',
                'origin': 0x0000a2e8,
                'size': 0x88,
                'path': 'c:/freescale/kds_v3/toolchain/bin/../lib/gcc/arm-none-eabi/4.8.4/armv6-m',
                'object': '_udivsi3.o',
                'library': 'libgcc.a',
                'symbol': '__udivsi3',
                'fill': 0
            },
            {
                'segment': 'text',
                'origin': 0x0000a370,
                'size': 0x14,
                'path': 'c:/freescale/kds_v3/toolchain/bin/../lib/gcc/arm-none-eabi/4.8.4/armv6-m',
                'object': '_udivsi3.o',
                'library': 'libgcc.a',
                'symbol': '__aeabi_uidivmod',
                'fill': 0
            },
            {
                'segment': 'text',
                'origin': 0x0000a388,
                'size': 0x80,
                'path': './2_Programa/source',
                'object': 'app_scheduler.o',
                'library': 'application',
                'symbol': 'APP_SCH_init',
                'fill': 0
            },
            {
                'segment': 'text',
                'origin': 0x0000a63c,
                'size': 0x15,
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/root/libs/bit_utils/v1_8',
                'object': 'bit_utils.o',
                'library': 'libbit_utils_v1_8.a',
                'symbol': 'BU_memset',
                'fill': 3
            },
            {
                'segment': 'text',
                'origin': 0x0000aa50,
                'size': 0x1c,
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/root/libs/scheduler/v0_6',
                'object': 'scheduler.o',
                'library': 'libscheduler_v0_6.a',
                'symbol': 'SCH_isEmpty',
                'fill': 0
            },
            {
                'segment': 'grot',
                'origin': 0x00038000,
                'size': 0x4,
                'path': './2_Programa/source',
                'object': 'objects.o',
                'library': 'application',
                'symbol': 'GLB_Grot',
                'fill': 0
            },
            {
                'segment': 'save',
                'origin': 0x20005000,
                'size': 0x4,
                'path': './2_Programa/source',
                'object': 'objects.o',
                'library': 'application',
                'symbol': 'GLB_objects',
                'fill': 0
            },
            {
                'segment': 'ARM',
                'origin': 0x00000032,
                'size': 0x32,
                'path': './2_Programa/source',
                'object': 'app_scheduler.o',
                'library': 'application',
                'symbol': 'attributes',
                'fill': 0
            }
        ]

        with open(get_asset_file('linker_script_and_memory_map.map'), 'r') as fd:
            mapfile_loader = MapFileAnalyzer()
            mapfile_loader._process_memory_map(fd.read())

            assert mapfile_loader.app_files == expected_app_files
            assert mapfile_loader.stack['size'] == expected_stack
            assert mapfile_loader.heap['size'] == expected_heap
            assert mapfile_loader.memory_segments == expected_memory_segments
            assert mapfile_loader.symbols_information == expected_objects_information

    def test_process_cross_reference(self):
        expected = [
            {
                'symbol': 'AC_requestAccess',
                'path': './2_Programa/source',
                'object': 'access_control.o',
                'library': 'application'
            },
            {
                'symbol': 'AC_requestAccess',
                'path': './2_Programa/source',
                'object': 'application.o',
                'library': 'application'
            },
            {
                'symbol': 'ADC0_IRQHandler',
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/root/libs/NxpNfcRdLib/v0_1',
                'object': 'startup_MKL26Z4.o',
                'library': 'libNxpNfcRdLib_v0_1.a'
            },
            {
                'symbol': 'ADC16_ClearStatusFlags',
                'path': './2_Programa/build/KDS/sdk',
                'object': 'fsl_adc16.o',
                'library': 'application'
            },
            {
                'symbol': 'BIN_configInput',
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/root/libs/binary_input/v0_3_12',
                'object': 'binary_input.o',
                'library': 'libbinary_input_v0_3_12.a'
            },
            {
                'symbol': 'BIN_configInput',
                'path': './2_Programa/source',
                'object': 'ad_inputs.o',
                'library': 'application'
            },
            {
                'symbol': 'BIN_getObjValue',
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/root/libs/binary_input/v0_3_12',
                'object': 'binary_input.o',
                'library': 'libbinary_input_v0_3_12.a'
            },
            {
                'symbol': 'BIN_getObjValue',
                'path': 'C:/WoozyCoder/Workspaces/KDS_v3/root/libs/binary_input/v0_3_12',
                'object': 'sensor.o',
                'library': 'libbinary_input_v0_3_12.a'
            }
        ]

        with open(get_asset_file('cross_reference_table.map'), 'r') as fd:
            mapfile_loader = MapFileAnalyzer()
            mapfile_loader._process_cross_reference(fd.read())

            assert mapfile_loader.cross_references == expected

    def test_process_memory_map_common_symbols(self):
        expected = [
            {
                'name': 'GLB_appIntObjPropChangeFlags',
                'origin': 0x20002b19,
                'fill': 0
            },
            {
                'name': 'GLB_aioBLCommand',
                'origin': 0x20002b1a,
                'fill': 0
            },
            {
                'name': 'GLB_aioDateTime',
                'origin': 0x20002b65,
                'fill': 0
            },
            {
                'name': 'GLB_goFlags',
                'origin': 0x20002ba0,
                'fill': 0x3
            },
            {
                'name': 'GLB_MS_ctx',
                'origin': 0x20002bdc,
                'fill': 0
            },
            {
                'name': 'errno',
                'origin': 0x20002fdc,
                'fill': 0
            }
        ]

        with open(get_asset_file('memory_map_common_symbols.map'), 'r') as fd:
            mapfile_loader = MapFileAnalyzer()
            mapfile_loader._process_memory_map_common_symbols(fd.read())

            assert mapfile_loader.common_symbols == expected

    def test_find_attribute_element_not_found(self):
        mapfile_loader = MapFileAnalyzer()

        mapfile_loader.common_symbols = [
            {
                'name': 'Common Symbol 1',
                'origin': 0,
                'size': 0,
                'path': '/path/to/object_file',
                'object': 'object_name',
                'library': 'library_name',
                'fill': 0,
            }
        ]

        result = mapfile_loader._find_attribute_element(mapfile_loader.common_symbols, 'invalid', 'do_not_care')

        assert result == []

    def test_find_attribute_element_found_one(self):
        mapfile_loader = MapFileAnalyzer()

        expected = [
            {
                'name': 'Common Symbol 1',
                'origin': 0,
                'size': 0,
                'path': '/path/to/object_file',
                'object': 'object_name',
                'library': 'library_name',
                'fill': 0,
            }
        ]

        mapfile_loader.common_symbols = list(expected)  # Copy the list instead of reference it

        result = mapfile_loader._find_attribute_element(mapfile_loader.common_symbols, 'object', 'object_name')

        assert isinstance(result, dict)
        assert result == expected[0]

    def test_find_attribute_element_found_multiple(self):
        mapfile_loader = MapFileAnalyzer()

        expected = [
            {
                'name': 'Common Symbol 1',
                'origin': 0,
                'size': 0,
                'path': '/path/to/object_file',
                'object': 'object_name',
                'library': 'library_name',
                'fill': 0,
            },
            {
                'name': 'Common Symbol 2',
                'origin': 0,
                'size': 0,
                'path': '/path/to/object_file',
                'object': 'object_name',
                'library': 'library_name',
                'fill': 0,
            }
        ]

        mapfile_loader.common_symbols = list(expected)  # Copy the list instead of reference it

        result = mapfile_loader._find_attribute_element(mapfile_loader.common_symbols, 'object', 'object_name')

        assert isinstance(result, list)
        assert result == expected
