# -*- coding: utf-8 -*-

from .mapser import MapFileAnalyzer

__all__ = ['MapFileAnalyzer']
